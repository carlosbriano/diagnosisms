package com.softtek.covid.diagnosisAPI.model;

public class Question {
	
	private String explanation;
	private Extra extras;
	private Item[] items;
	private String text;
	private String type;
	
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
//	public String getExtras() {
//		return extras;
//	}
//	public void setExtras(String extras) {
//		this.extras = extras;
//	}
	public Item[] getItems() {
		return items;
	}
	public void setItems(Item[] items) {
		this.items = items;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
