package com.softtek.covid.diagnosisAPI.model;

public class Data {
	
	private Translation[] translations;

	public Translation[] getTranslations() {
		return translations;
	}

	public void setTranslations(Translation[] translations) {
		this.translations = translations;
	}
	
}
