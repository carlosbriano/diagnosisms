package com.softtek.covid.diagnosisAPI.model;

public class DiagnosisRequest {
	
	private String sex; 
	private Integer age;
	private Evidence [] evidence;
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Evidence[] getEvidence() {
		return evidence;
	}
	public void setEvidence(Evidence[] evidence) {
		this.evidence = evidence;
	}
	

}
