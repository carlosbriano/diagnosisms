package com.softtek.covid.diagnosisAPI.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = { "extras" })
public class DiagnosisResponse {
	
	private String[] conditions;
	private Extra extras;  //check 
	private Question question;
	private String shoud_stop;
	
	public String[] getConditions() {
		return conditions;
	}
	
	public void setConditions(String[] conditions) {
		this.conditions = conditions;
	}
	
	
	public Question getQuestion() {
		return question;
	}
	
	public void setQuestion(Question question) {
		this.question = question;
	}
	

	public Extra getExtras() {
		return extras;
	}

	public void setExtras(Extra extras) {
		this.extras = extras;
	}

	public String getShoud_stop() {
		return shoud_stop;
	}

	public void setShoud_stop(String shoud_stop) {
		this.shoud_stop = shoud_stop;
	}
	
}
