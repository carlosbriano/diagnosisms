package com.softtek.covid.diagnosisAPI.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.softtek.covid.diagnosisAPI.model.DiagnosisRequest;
import com.softtek.covid.diagnosisAPI.model.DiagnosisResponse;
import com.softtek.covid.diagnosisAPI.model.GoogleResponse;

public class DiagnosisService {
	
	public static DiagnosisResponse  callDiagnosisApi(DiagnosisRequest diagnosis){
		HttpHeaders headers = new HttpHeaders();
		headers.set("App-Id", "f70abc4e");
		headers.set("App-Key", "a1903c5c359c0260814e0348cadeb344");
		headers.set("Content-Type", "application/json");
		HttpEntity<DiagnosisRequest> entity = new HttpEntity<DiagnosisRequest>(diagnosis,headers);
		RestTemplate restTemplate=new RestTemplate();
		//DiagnosisResponse diagnosisRes= restTemplate.postForObject("https://api.infermedica.com/covid19/diagnosis" ,diagnosis, DiagnosisResponse.class );
		ResponseEntity<DiagnosisResponse> diagnosisRes = restTemplate.exchange("https://api.infermedica.com/covid19/diagnosis", HttpMethod.POST, entity, DiagnosisResponse.class);
		return diagnosisRes.getBody();
	}

}
