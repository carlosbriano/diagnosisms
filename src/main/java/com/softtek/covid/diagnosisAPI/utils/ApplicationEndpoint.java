package com.softtek.covid.diagnosisAPI.utils;

public class ApplicationEndpoint {
	public static final String TRANSLATOR= "https://translation.googleapis.com/language/translate/v2";
	public static final String DIAGNOSIS="https://api.infermedica.com/covid19";
}
